P=find
OBJECTS=utils.o
CFLAGS = -g -Wall -O3 `pkg-config --cflags glib-2.0`
LDLIBS=`pkg-config --libs glib-2.0`
CC=gcc

$(P): $(OBJECTS)
