int is (char *str1, char *str2);
void handle_no_arg(int index, int length, char* arg);
int is_int(char *val, int *int_val);
char* lower(char *str);

struct node {
  char* name;
  int was_seen;
  int depth;
};
