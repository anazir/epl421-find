#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

int is (char *str1, char *str2) {
  return strcmp(str1, str2) == 0;
}

void handle_no_arg(int index, int length, char* arg) {
  if (index == length) {
    printf("find: missing argument to `%s'\n", arg);
    exit(1);
  }
}

int is_int(char *val, int *int_val) {
  char* temp;
  *int_val = strtol(val, &temp, 10);

  // Check for empty string and characters left after conversion.
  if ((temp == val) || (*temp != '\0')) {
    return 0;
  }

  return 1;
}

char* lower(char *str) {
  char *new_str = strdup(str);

  for (int i = 0; i < strlen(str); i++) {
    new_str[i] = tolower(str[i]);
  }

  return new_str;
}
