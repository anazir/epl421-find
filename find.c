#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <pwd.h>
#include <grp.h>
#include <glib.h>
#include <dirent.h>
#include <sys/stat.h>
#include <fnmatch.h>
#include "utils.h"

void find (char *path, char type, int uid, int has_uid, int gid, int has_gid, char *name, int case_sensitive, int inum, int has_inum, int depth, int maxdepth, int has_maxdepth, int mindepth, int has_mindepth) {
  GQueue *queue = g_queue_new();
  struct node *root = calloc(1, sizeof(struct node));
  root->name = path;
  root->was_seen = 0;
  root->depth = 0;
  g_queue_push_tail(queue, root);

  while (!g_queue_is_empty(queue)) {
    struct node *cur = g_queue_pop_tail(queue);
    int should_print = 1, should_free = 1;

    struct stat *buf = calloc(1, sizeof(struct stat));
    lstat(cur->name, buf);

    if (has_uid && buf->st_uid != uid) {
      should_print = 0;
    }

    if (has_gid && buf->st_gid != gid) {
      should_print = 0;
    }

    if (has_inum && buf->st_ino != inum) {
      should_print = 0;
    }

    if (has_mindepth && cur->depth < mindepth) {
      should_print = 0;
    }

    char* filename = strrchr(cur->name, '/');
    if (filename == NULL) {
      filename = cur->name;
    } else {
      filename ++;
    }

    if (!case_sensitive) {
      filename = lower(filename);
      name = lower(name);
    }

    if (name != NULL && fnmatch(name, filename, 0)) {
      should_print = 0;
    }

    DIR *dir = opendir(cur->name);

    switch (type) {
      case 'd':
        should_print = dir == NULL ? 0 : should_print;
        break;
      case 'f':
        should_print = dir != NULL ? 0 : should_print;
        break;
      case 'l':
        should_print = !S_ISLNK(buf->st_mode) ? 0 : should_print;
        break;
      case 'b':
        should_print = !S_ISBLK(buf->st_mode) ? 0 : should_print;
        break;
      case 'c':
        should_print = !S_ISCHR(buf->st_mode) ? 0 : should_print;
        break;
      case 'p':
        should_print = !S_ISFIFO(buf->st_mode) ? 0 : should_print;
        break;
      case 's':
        should_print = !S_ISSOCK(buf->st_mode) ? 0 : should_print;
        break;
      case 'D':
        should_print = !S_ISSOCK(buf->st_mode) ? 0 : should_print;
        break;
    }

    free(buf);

    // If directory
    if (dir != NULL && !cur->was_seen && (!has_maxdepth || maxdepth >= cur->depth + 1)) {
      if (depth) {
        cur->was_seen = 1;
        g_queue_push_tail(queue, cur);
        should_print = 0;
        should_free = 0;
      }

      struct dirent *dir_entry;
      while ((dir_entry = readdir(dir)) != NULL) {
        if (is(dir_entry->d_name, ".") || is(dir_entry->d_name, "..")) {
          continue;
        }

        char *new_name = calloc(strlen(cur->name) + 2 + strlen(dir_entry->d_name), sizeof(char));
        strcpy(new_name, cur->name);
        strcat(new_name, "/");
        strcat(new_name, dir_entry->d_name);
        struct node *new = calloc(1, sizeof(struct node));
        new->name = new_name;
        new->was_seen = 0;
        new->depth = cur->depth + 1;
        g_queue_push_tail(queue, new);
      }
    }
    closedir(dir);

    if (should_print) {
      printf("%s\n", cur->name);
    }

    if (should_free) {
      free(cur->name);
      free(cur);
    }
  }

  g_queue_free(queue);
}

int main (int argc, char *argv[]) {
  int symbolic_link_behaviour = 2;
  int c = 1;
  char *path = ".", *name = NULL;
  char type;
  int uid, has_uid = 0, gid, has_gid = 0, case_sensitive = 1, maxdepth;
  int depth_first = 0, ignore_readdir_race = 0, mount = 1, inum, has_inum = 0;
  int mindepth, has_maxdepth = 0, has_mindepth = 0;

  // Get -H -P or -L
  while (argc >= 2 && (is(argv[c], "-H") || is(argv[c], "-L") || is(argv[c], "-P"))) {
    switch (argv[c][1]) {
      case 'H':
        symbolic_link_behaviour = 0;
        break;
      case 'L':
        symbolic_link_behaviour = 1;
        break;
      case 'P':
        symbolic_link_behaviour = 2;
        break;
    }

    c ++;
  }

  // Get path
  if (argc == c) {
    path = ".";
  } else if (argv[c][0] != '-' || strlen(argv[c]) == 1) {
    path = argv[c];
    c ++;
  }

  for (; c < argc; c ++) {
    // version
    if (is(argv[c], "-version") || is(argv[c], "--version")) {
      printf("find 1.0.0\n");
      exit(0);
    }

    // help
    if (is(argv[c], "-help") || is(argv[c], "--help")) {
      printf("Usage: find [-H] [-L] [-P] [path...] [expression]\n\n");
      printf("Default path is the current directory; default expression is -print.\n");
      printf("Expression may consist of: options.\n\n");
      printf("Normal options:\n");
      printf("\t-type c\n");
      printf("\t\tFile is of type c:\n");
      printf("\t\tb\tblock (buffered) special\n");
      printf("\t\tc\tcharacter (unbuffered) special\n");
      printf("\t\td\tdirectory\n");
      printf("\t\tp\tnamed pipe (FIFO)\n");
      printf("\t\tf\tregular file\n");
      printf("\t\tl\tsymbolic link; this is never true if the -L option or the -follow\n");
      printf("\t\t\toption is in effect, unless the symbolic link is broken. If you\n");
      printf("\t\t\twant to search for symbolic links when -L is in effect,\n");
      printf("\t\t\tuse -xtype.\n");
      printf("\t\ts\tsocket\n");
      printf("\t\tD\tdoor (Solaris)\n\n");
      printf("\t-uid n\n");
      printf("\t\tFile’s numeric user ID is n.\n\n");
      printf("\t-user uname\n");
      printf("\t\tFile is owned by user uname (numeric user ID allowed).\n\n");
      printf("\t-gid n\n");
      printf("\t\tFile’s numeric group ID is n.\n\n");
      printf("\t-group gname\n");
      printf("\t\tFile belongs to group gname (numeric group ID allowed).\n\n");
      printf("\t-name pattern\n");
      printf("\t\tBase of file name (the path with the leading directories removed)\n");
      printf("\t\tmatches shell pattern pattern. The metacharacters (‘*’, ‘?’, and ‘[]’)\n");
      printf("\t\tmatch a ‘.’ at the start of the base name. The filename matching is\n");
      printf("\t\tperformed with the use of the fnmatch(3) library function. Don’t forget\n");
      printf("\t\tto enclose the pattern in quotes in order to protect it from expansion\n");
      printf("\t\tby the shell.\n\n");
      printf("\t-iname pattern\n");
      printf("\t\tLike -name, but the match is case insensitive. For example, the patterns\n");
      printf("\t\t‘fo*’ and ‘F??’ match the file names ‘Foo’, ‘FOO’, ‘foo’, ‘fOo’, etc.\n");
      printf("\t\tIn these patterns, unlike filename expansion by the shell, an initial ‘.’\n");
      printf("\t\tcan be matched by ‘*’. That is, find -name *bar will match the file\n");
      printf("\t\t‘.foo-bar’. Please note that you should quote patterns as a matter of\n");
      printf("\t\tcourse, otherwise the shell will expand any wildcard characters in them.\n\n");
      printf("\t-inum n\n");
      printf("\t\tFile has inode number n. It is normally easier to use the -samefile test\n");
      printf("\t\tinstead.\n\n");
      printf("\t-depth\n");
      printf("\t\tProcess each directory’s contents before the directory itself (i.e.,\n");
      printf("\t\tDepth-First-Search using a Stack).\n\n");
      printf("\t-ignore_readdir_race\n");
      printf("\t\tNormally, find will emit an error message when it fails to stat a file.\n");
      printf("\t\tIf you give this option and a file is deleted between the time find\n");
      printf("\t\treads the name of the file from the directory and the time\n");
      printf("\t\tit tries to stat the file, no error message will be issued. This also\n");
      printf("\t\tapplies to files or directories whose names are given on the command\n");
      printf("\t\tline. This option takes effect at the time the command line is read,\n");
      printf("\t\twhich means that you cannot search one part of the filesystem with this\n");
      printf("\t\toption on and part of it with this option off (if you need to do\n");
      printf("\t\tthat, you will need to issue two find commands instead, one with the\n");
      printf("\t\toption and one without it).\n\n");
      printf("\t-maxdepth levels\n");
      printf("\t\tDescend at most levels (a non-negative integer) levels of directories\n");
      printf("\t\tbelow the command line arguments. -maxdepth 0 means only apply the\n");
      printf("\t\ttests and actions to the command line arguments.\n\n");
      printf("\t-mindepth levels\n");
      printf("\t\tDo not apply any tests or actions at levels less than levels (a non\n");
      printf("\t\tnegative integer). -mindepth 1 means process all files except the\n");
      printf("\t\tcommand line arguments.\n\n");
      printf("\t-mount\n");
      printf("\t\tDon’t descend directories on other filesystems (please read local\n");
      printf("\t\tfilesystem prefixes from /etc/fstab). An alternate name for -xdev, for\n");
      printf("\t\tcompatibility with some other versions of find.\n\n");
      printf("\t-xdev\n");
      printf("\t\tDon’t descend directories on other filesystems.\n\n");
      printf("Other common options:\n");
      printf("\t--help\t\t\tdisplay this help and exit\n");
      printf("\t--version\t\toutput version information and exit\n");
      exit(0);
    }

    // type
    if (is(argv[c], "-type")) {
      c ++;
      handle_no_arg(c, argc, "-type");

      if (strlen(argv[c]) > 1) {
        printf("find: only one character allowed as argument for argument -type\n");
      }

      switch (argv[c][0]) {
        case 'b':
        case 'c':
        case 'd':
        case 'p':
        case 'f':
        case 'l':
        case 's':
        case 'D':
          type = argv[c][0];
          break;
        default:
          printf("find: unknown argument to -type: %c\n", argv[c][0]);
          break;
      }
      continue;
    }

    // uid
    if (is(argv[c], "-uid")) {
      c ++;
      handle_no_arg(c, argc, "-uid");

      if (!is_int(argv[c], &uid)) {
        printf("find: invalid argument `%s' to `-uid'\n", argv[c]);
        exit(1);
      } else {
        has_uid = 1;
      }
      continue;
    }

    // user
    if (is(argv[c], "-user")) {
      c ++;
      handle_no_arg(c, argc, "-user");

      if (!is_int(argv[c], &uid)) {
        struct passwd *user = getpwnam(argv[c]);

        if (user == NULL) {
          printf("find: ‘%s’ is not the name of a known user\n", argv[c]);
          exit(1);
        }

        uid = user->pw_uid;
      }
      has_uid = 1;
      continue;
    }

    // gid
    if (is(argv[c], "-gid")) {
      c ++;
      handle_no_arg(c, argc, "-gid");

      if (!is_int(argv[c], &gid)) {
        printf("find: invalid argument `%s' to `-gid'\n", argv[c]);
        exit(1);
      }
      has_gid = 1;
      continue;
    }

    // group
    if (is(argv[c], "-group")) {
      c ++;
      handle_no_arg(c, argc, "-group");

      if (!is_int(argv[c], &gid)) {
        struct group *gr = getgrnam(argv[c]);

        if (gr == NULL) {
          printf("find: ‘%s’ is not the name of an existing group\n", argv[c]);
          exit(1);
        }

        gid = gr->gr_gid;
      }
      has_gid = 1;
      continue;
    }

    // name
    if (is(argv[c], "-name")) {
      c ++;
      handle_no_arg(c, argc, "-name");
      name = argv[c];
      continue;
    }

    // iname
    if (is(argv[c], "-iname")) {
      c ++;
      handle_no_arg(c, argc, "-iname");
      name = argv[c];
      case_sensitive = 0;
      continue;
    }

    // inum
    if (is(argv[c], "-inum")) {
      c ++;
      handle_no_arg(c, argc, "-inum");

      if (!is_int(argv[c], &inum)) {
        printf("find: invalid argument `%s' to `-inum'\n", argv[c]);
        exit(1);
      }

      has_inum = 1;
      continue;
    }

    // depth
    if (is(argv[c], "-depth")) {
      depth_first = 1;
      continue;
    }

    // ignore_readdir_race
    if (is(argv[c], "-ignore_readdir_race")) {
      ignore_readdir_race = 1;
      continue;
    }

    // maxdepth
    if (is(argv[c], "-maxdepth")) {
      c ++;
      handle_no_arg(c, argc, "-maxdepth");

      if (!is_int(argv[c], &maxdepth) || maxdepth < 0) {
        printf("find: Expected a positive decimal integer argument to -maxdepth, but got ‘%s’\n", argv[c]);
        exit(1);
      }
      has_maxdepth = 1;
      continue;
    }

    // mindepth
    if (is(argv[c], "-mindepth")) {
      c ++;
      handle_no_arg(c, argc, "-mindepth");

      if (!is_int(argv[c], &mindepth) || mindepth < 0) {
        printf("find: Expected a positive decimal integer argument to -mindepth, but got ‘%s’\n", argv[c]);
        exit(1);
      }
      has_mindepth = 1;
      continue;
    }

    // mount, xdev
    if (is(argv[c], "-mount") || is(argv[c], "-xdev")) {
      mount = 0;
      continue;
    }

    // handle unwanted input
    printf("find: paths must precede expression: `%s'\n", argv[c]);
    exit(1);
  }

  find(strdup(path), type, uid, has_uid, gid, has_gid, name, case_sensitive, inum, has_inum, depth_first, maxdepth, has_maxdepth, mindepth, has_mindepth);

  return 0;
}
